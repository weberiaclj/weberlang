;; in the meantime, it was blatantly stolen from:
;; https://gist.github.com/pooster/6221672

(ns weberlang.parser
    (:require [instaparse.core :as insta]
              [org.httpkit.client :as http-kit]))

(def parser
     (insta/parser
      (clojure.java.io/resource "ifa-grammar.bnf")))

(defn request-resources [url]
  (let [response (http-kit/get url)]
    (println "Status: " (:status @response))))

(def transform-options
     {:statement (request-resources "http://clojure.org")})
;;  {:statement (request-resources :url)})

(defn parse [input]
     (->> (parser input) (insta/transform transform-options)))
