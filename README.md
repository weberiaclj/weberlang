# Project Weberia - WeberLang

## Usage

* Get [Clojure[(http://www.clojure.org)
* Get [Leiningen](http://www.leiningen.org) to work
* inside this directory: `lein run`
* Try some simple arithmetic inside S-Exp: (+ 32 (* 2 3))

## License

Copyright © 2015 [Bambang Purnomosidi D. P.](http://bpdp.name)

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
