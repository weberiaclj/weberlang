(defproject weberlang "1.0.0"
  :description "Implementation of IFA - Interaction for Action language specification"
  :url "http://github.com/weberia/weberlang"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main weberlang.repl
  :repositories [["sonatype" "https://oss.sonatype.org/content/repositories/releases"]
                 ["apache" "http://repository.apache.org/content/repositories/releases"]]
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [instaparse "1.4.1"]
                 [http-kit "2.1.19"]])
