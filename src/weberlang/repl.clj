(ns weberlang.repl
  (:require [weberlang.parser :as parser]))

(defn repl []
  (do
    (print "WeberLang> ")
    (flush))
  (let [input (read-line)]
    (if (= input ":q") (System/exit 0) (println (parser/parse input)))
    (recur)))

(defn -main [& args]
  (println "o       o")
  (println "|       |")
  (println "o   o   o")
  (println " \\ / \\ / ")
  (println "  o   o  ")
  (println)
  (flush)
  (repl))
